import React, { Component } from "react";
import "./App.css";
import Header from "./Header/Header";
import { Button, TextField } from "@material-ui/core";
import Todo from "./Todo/Todo";
import db from "./firebase";
import firebase from "firebase";

class App extends Component {
  state = {
    input: "",
    todos: [{ text: "walk a dog", id: "1234" }],
  };
  componentDidMount() {
    db.collection("todos").onSnapshot((snapshot) => {
      this.setState({
        todos: snapshot.docs.map((doc) => ({
          text: doc.data().todo,
          id: doc.id,
        })),
      });
    });
  }

  handleInputChanged = (event) => {
    this.setState({
      input: event.target.value,
    });
  };
  handleAddButtonClicked = (event) => {
    event.preventDefault();
    db.collection("todos").add({
      todo: this.state.input,
      timestamp: firebase.firestore.FieldValue.serverTimestamp(),
    });
    this.setState({
      input: "",
    });
  };
  render() {
    return (
      <div className="app">
        <Header />
        {/* input and button */}

        <form className="app__form">
          <TextField
            label="Write a Todo"
            variant="outlined"
            value={this.state.input}
            onChange={this.handleInputChanged}
          />
          <Button
            type="submit"
            variant="contained"
            color="primary"
            onClick={this.handleAddButtonClicked}
            disabled={!this.state.input}
          >
            Add
          </Button>
        </form>

        <div className="app__todo">
          {this.state.todos.map((todo, index) => (
            <Todo key={index} todo={todo} />
          ))}
        </div>
        {/* footer */}
      </div>
    );
  }
}

export default App;
