import React, { useState } from "react";
import { Paper, Button } from "@material-ui/core";
import "./Todo.css";
import db from "../firebase";
import firebase from "firebase";
import Modal from "../Modal";

function Todo({ todo }) {
  const [open, setOpen] = useState(false);

  const handleClose = () => {
    setOpen(false);
  };
  const editModal = (id, input) => {
    db.collection("todos").doc(id).set({
      todo: input,
      timestamp: firebase.firestore.FieldValue.serverTimestamp(),
    });
    setOpen(false);
  };
  const handleEditClicked = (todo) => {
    setOpen(true);
  };
  const handleDeleteClicked = (id) => {
    db.collection("todos").doc(id).delete();
  };
  return (
    <div className="todo">
      <Modal
        open={open}
        handleClose={handleClose}
        todo={todo}
        editModal={editModal}
      />
      <Paper className="todo__paper">
        <p className="todo__text">{todo.text}</p>

        <Button color="primary" onClick={() => handleEditClicked(todo)}>
          Edit
        </Button>
        <Button color="secondary" onClick={() => handleDeleteClicked(todo.id)}>
          Delete
        </Button>
      </Paper>
    </div>
  );
}

export default Todo;
