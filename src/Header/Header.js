import React, { Component } from "react";
import "./Header.css";

import Quote from "popular-movie-quotes";

class Header extends Component {
  state = {
    randomQuote: "",
  };
  componentDidMount() {
    console.log("component mounter");
    this.setState({
      randomQuote: Quote.getRandomQuote(),
    });
  }
  render() {
    return (
      <div className="header">
        <img className="header__img" src="/todoHeader.png" alt="header_image" />

        <p>{this.state.randomQuote}</p>
      </div>
    );
  }
}

export default Header;
